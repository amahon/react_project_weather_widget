# Tech-Book-Club React Project

## Brief: Create a weather widget

### Acceptance Criteria 
* Must be able to select a location by typing one in
* Must display the details for that location on that day
* Must use data from beta, e.g.  http://www-beta.localsearch.com.au/api/data/v2/forecasts?filter[suburb]=robina
* Must be live data
* Must use personal gitlab to upload it