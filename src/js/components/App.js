var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var SearchForm = require('./SearchForm.js');
var WeatherResults = require('./WeatherResults.js');


function getAppState(){
  return {
    weather: AppStore.getWeatherResults()
  }
}


// Other components that are created have to be required in here.
var App = React.createClass({
  getInitialState: function(){
    return getAppState();
  },
  componentDidMount: function(){
    AppStore.addChangeListener(this._onChange);
  },
  componentWillUnmount: function(){
    AppStore.removeChangeListener(this._onChange);
  },


  render: function () {
    if(this.state.weather == ''){
      var weatherResults ='';
    } else {
      var weatherResults = <WeatherResults
          weather = {this.state.weather}/>;
    }
    return (
      <div>
        <SearchForm />
        {weatherResults}
      </div>
    );
  },

  _onChange: function(){
    this.setState(getAppState());
  }
});

module.exports = App;