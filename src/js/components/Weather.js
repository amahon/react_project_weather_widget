var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');


var Weather = React.createClass({
    render: function () {
        function shortName(item) {
            var full_name = item.props.weather.attributes.current[0].icon_filename;
            var name = full_name.split(".");
            return "wi " + name[0];
        };

        function timeFormat(datetimestr) {
            var str = datetimestr.split("T");
            var string = str[1].substr(0, 5)
            var postfix;
            if (Number(string.substr(0, 2)) < 12) {
                postfix = "am"
            } else {
                postfix = "pm"
            }
            if (string.charAt(0) === '0') {
                string = string.slice(1);
            }
            return string + " " + postfix;
        }

        return (
            <div className="well text-center">
                <div className="row">
                    <h3> {this.props.weather.attributes.suburb} for {this.props.weather.attributes.current[0].day_name}</h3>
                    <hr className="style-seven"/>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <h1 className={ shortName(this)}></h1>

                        <h2>{this.props.weather.attributes.current[0].temperature} <i className="wi wi-celsius"></i>
                        </h2>

                        <h3 className="blue">{this.props.weather.attributes.current[0].icon_phrase}</h3>
                    </div>
                    <div className="col-lg-6">
                        <div className="col-lg-6 text-center">
                            <h3><span className="blue">Min:</span> <i
                                className="wi wi-direction-down"></i> {this.props.weather.attributes.current[0].min}
                                <i className="wi wi-celsius"></i>
                            </h3>
                            <br />

                            <h3><span className="blue">Sunrise:</span> <i
                                className="wi wi-sunrise"></i></h3>
                            <h4>{timeFormat(this.props.weather.attributes.current[0].sunrise)}</h4>
                        </div>
                        <div className="col-lg-6 text-center">
                            <h3><span className="red">Max:</span> <i
                                className="wi wi-direction-up"></i> {this.props.weather.attributes.current[0].max}
                                <i className="wi wi-celsius"></i>
                            </h3>
                            <br />

                            <h3><span className="blue">Sunset:</span> <i
                                className="wi  wi-sunset"></i></h3>
                            <h4>{timeFormat(this.props.weather.attributes.current[0].sunset)}</h4>
                        </div>
                    </div>
                </div>


            </div>
        );
    }

});

module.exports = Weather;