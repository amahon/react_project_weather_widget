var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');

// Other components that are created have to be required in here.
var SearchForm = React.createClass({
  render: function () {
    return (
      <div className="search-form">
        <h1 className="text-center">Weather Widget</h1>

        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <input type="text" className="form-control" ref="suburb" placeholder="Enter a suburb.." autoFocus/>
          </div>
          <button className="btn btn-primary btn-block">Search Weather</button>
        </form>
      </div>
    );
  },

  onSubmit: function(e){
    e.preventDefault();

    var suburb = {
      name: this.refs.suburb.value.trim()
    }

    AppActions.searchWeather(suburb);
  }
});

module.exports = SearchForm;