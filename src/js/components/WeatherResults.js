var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var Weather = require('./Weather');

// Other components that are created have to be required in here.
var WeatherResults = React.createClass({
  render: function () {
    return (
      <div>
      <h3 className="text-center">Results {this.props.weather[0].suburb}</h3>
        {
          this.props.weather.map(function(data, i){
            return (
              <Weather weather={data} key={i} />
            )
          })
        }
      </div>
    );
  }

});

module.exports = WeatherResults;