var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');


var AppActions = {
  searchWeather: function(suburb){
    AppDispatcher.handleViewAction({
      actionType: AppConstants.SEARCH_WEATHER,
      suburb: suburb
    });
  },
  receiveWeatherResults: function(data){
    AppDispatcher.handleViewAction({
      actionType: AppConstants.RECEIVE_WEATHER_RESULTS,
      data: data
    });
  }
};

module.exports = AppActions;