var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var AppAPI = require('../utils/appAPI');


var CHANGE_EVENT = 'change';

var _weather = [];
var _selected = '';

var AppStore = assign({}, EventEmitter.prototype, {
  setWeatherResults: function(weatherData){
    _weather = weatherData;
  },
  getWeatherResults: function(){
    return _weather;
  },
  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function (cb) {
    this.on('change', cb);
  },
  removeChangeListener: function(cb){
    this.removeListener('change', cb);
  }
});

AppDispatcher.register(function(payload){
  var action = payload.action;

  switch(action.actionType){
    case AppConstants.SEARCH_WEATHER:
      console.log("searching for weather : " + action.suburb.name );
      AppAPI.searchWeather(action.suburb)
      AppStore.emit(CHANGE_EVENT);
      break;
    case AppConstants.RECEIVE_WEATHER_RESULTS:
     AppStore.setWeatherResults(action.data);
      AppStore.emit(CHANGE_EVENT);
      break;
  }

  return true;
});

module.exports = AppStore;