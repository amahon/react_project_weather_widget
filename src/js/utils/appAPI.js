var AppActions = require('../actions/AppActions');

module.exports = {
  searchWeather: function(suburb){
    $.ajax({
      url: 'http://www-beta.localsearch.com.au/api/data/v2/forecasts?filter[suburb]=' + suburb.name,
      dataType: 'json',
      cache:false,
      success: function(data){
        AppActions.receiveWeatherResults(data.data);
      }.bind(this),
      error: function(xhr, status, error){
        alert(error);
      }.bind(this)
    })
  }
};